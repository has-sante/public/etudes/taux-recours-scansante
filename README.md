# Taux de recours ScanSanté

## Description

L'étude du taux de recours aux actes médicaux pratiqués en hospitalisation est un enjeu important pour certains services de la HAS à des fins de contextualisation (ex: Service d'Evaluation des Dispositifs --SED). Ce projet produit des rapports statistiques automatiques sur les taux de recours à partir des données de l'ATIH publiées sur [scansanté](https://www.scansante.fr/applications/taux-de-recours-mco/). Le programme d’analyse prend en entrée les codes CCAM d'un groupe d'actes et produit un rapport jupyter/html/pdf.

Ce projet a été initié pour fournir des taux de recours aux bioprothèses aortiques (TAVI)  pour le rapport [Critères d’éligibilité des centres implantant des TAVIs (2023)](https://www.has-sante.fr/upload/docs/application/pdf/2023-12/rapport_tavi_version_definitive_2023-12-18_11-48-12_768.pdf). Il a impliqué le SED et la mission data de janvier à novembre 2023. Le contexte de l'analyse et le rapport sont disponibles sur [le site de documentation, onglet rapport](https://has-sante.pages.has-sante.fr/public/etudes/taux-recours-scansante/reports/index.html).


### Illustration : Cartographie du taux de recours au TAVI en France métropolitaine en 2022

![](/_static/img/illustration_taux_recours_scansante.png)

## Contexte technique

### Données utilisées, accès, sécurité

Les données sont téléchargées depuis le site ScanSanté, et sont publiques. Une documentation du cycle de vie des données est disponible sur le site de documentation, [onglet données](https://has-sante.pages.has-sante.fr/public/etudes/taux-recours-scansante/data.html).

### Stack technique

Python est utilisé pour toute la pipeline. Le versioning est géré avec pyenv et poetry et une entrée [cli](https://click.palletsprojects.com/en/8.1.x/) permet une utilisation en ligne de commande. Le code se trouve dans le dossier [taux_recours_scansante](https://gitlab.has-sante.fr/has-sante/public/etudes/taux-recours-scansante/-/tree/main/taux_recours_scansante?ref_type=heads). 


### Besoin de maintenance 

Projet finalisé, sans maintenance ni modification (sauf si décision d'évolutions). 

## Liens utiles 

- **Guide utilisateur** : [https://has-sante.pages.has-sante.fr/public/etudes/taux-recours-scansante/usage.html](https://has-sante.pages.has-sante.fr/public/etudes/taux-recours-scansante/usage.html)

- **Documentation** : [https://has-sante.pages.has-sante.fr/public/etudes/taux-recours-scansante/](https://has-sante.pages.has-sante.fr/public/etudes/taux-recours-scansante/)

- **Code source** : [https://gitlab.has-sante.fr/has-sante/public/etudes/taux-recours-scansante](https://gitlab.has-sante.fr/has-sante/public/etudes/taux-recours-scansante)


## Contacts 

- Mission data : responsable données de santé pour la contextualisation