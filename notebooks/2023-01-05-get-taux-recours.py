# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.14.4
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# %%
from os import path

import numpy as np
import pandas as pd
import requests

from taux_recours_scansante.constants import ACTES_TAVI

pd.options

pd.set_option("display.max_columns", 30)
pd.set_option("display.max_rows", 150)

# %%
secure_data = False  # Travailler depuis www.scansante.fr ou depuis secure.scansante.fr

# %%
actes_dict = ACTES_TAVI
niveaux_geo = ["dep", "reg"]
versions_liste = [
    "v2016",
    "v2021",
]  # v2021 pour période 2017-2021. v2016 pour période 2013-2016


# %%
def get_acte_groupe(acte: str):
    for groupe_acte, liste_acte in actes_dict.items():
        if acte in liste_acte:
            return groupe_acte
    return None


get_acte_groupe("DBLF001")
get_acte_groupe("DGKA011")


# %%
def iter_acte_geo_version():
    for groupe_acte, liste_actes in actes_dict.items():
        for acte in liste_actes:
            for geo in niveaux_geo:
                for version in versions_liste:
                    yield acte, geo, version


# %%
def get_filepath(acte, geo, version):
    if secure_data:
        return f"../data/taux-recours-secure-{acte}-{geo}-{version}.html"
    else:
        return f"../data/taux-recours-{acte}-{geo}-{version}.html"


def get_url(acte, geo, version):
    base_url_part = "www"
    if secure_data:
        base_url_part = "secure"
    return (
        f"https://{base_url_part}.scansante.fr/applications/taux-de-recours-mco/submit?snatnav=&mbout=part1&"
        + f"champ=MCO&unite=s%25E9jour&version={version}&taux=stand&tgeo={geo}&stat1=&IPA=&ASO=&CAS=&DA=&GP=&GA=&RAC=&"
        + f"type_rgp1=ACTE&S_F=&M_F=&Act={acte}&version2={version}&tgeo2=fe&codegeo=fe&stat2=&type_rgp2=tous"
    )


def download_html(acte, geo, version):
    filepath = get_filepath(acte, geo, version)
    url = get_url(acte, geo, version)

    if path.exists(filepath):
        print(f"Le fichier '{filepath}' existe déjà")
    else:
        print(f"Téléchargement de la page {url}")
        r = requests.get(url, headers={"User-Agent": "Mozilla/5.0 Firefox/100.0"})
        if r.status_code != 200:
            raise Exception(f"Erreur pour télécharger la page : {r.status_code}")
        print(f"Ecriture sur le fichier '{filepath}'")
        with open(filepath, "w") as f:
            f.write(r.text)


# %%
for param in iter_acte_geo_version():
    download_html(*param)

# %%
import os

# Lecture des tables, compte des lignes et suppression des fichiers en erreur
for acte, geo, version in iter_acte_geo_version():
    filepath = get_filepath(acte, geo, version)
    print(filepath)
    with open(filepath) as f:
        try:
            l = pd.read_html(f, decimal=",", thousands=" ")
            df = l[2]
            print(len(df), "lignes dans la table")
        except:
            print("erreur")
            os.remove(filepath)


# %%
def format_table(acte, geo, version):
    filepath = get_filepath(acte, geo, version)
    with open(filepath) as f:
        l = pd.read_html(f, decimal=",", thousands=" ")

    df = l[2]

    df["groupe_acte"] = get_acte_groupe(acte)
    df["code_acte"] = acte

    df["code_geo"] = df.iloc[:, 0].str.split(" - ").str[0]
    df.iloc[-1, -1] = "FR"

    df["libelle_geo"] = df.iloc[:, 0].str.split(" - ").str[1]
    df.iloc[-1, -1] = "France"

    df["niveau_geo"] = "Region" if geo == "reg" else "Departement"
    df.iloc[-1, -1] = "France"

    df = df.iloc[:, 1:].set_index(
        ["groupe_acte", "code_acte", "niveau_geo", "code_geo", "libelle_geo"]
    )
    df.columns.names = ["variable", "annee"]
    df = df.stack()

    # Tranformer 'annee' en entier
    df = df.reset_index("annee")
    df["annee"] = df["annee"].astype(int)
    df = df.set_index([df.index, df["annee"]])

    map_columns = {
        "Nombre de séjour": "nombre_sejour",
        "Taux de séjour Standardisé /1000 hab.": "taux_sejour",
        "Indice national": "indice_national",
        "Indice régional": "indice_regional",
    }

    df = df.rename(columns=map_columns)
    if geo == "reg":
        df["indice_regional"] = np.nan
    if geo == "dep":
        df.loc[
            df.index.get_level_values("niveau_geo") == "France",
            "indice_regional",
        ] = np.nan  # Correction erreur '0'

    df["taux_sejour"] = (
        df["taux_sejour"].replace("N.S.", np.nan).astype(float) * 100
    )  # taux standardisé /100000 hab.
    df["nombre_sejour"] = (
        df["nombre_sejour"].replace("1 à 10", np.nan).astype(float).astype("Int64")
    )
    df = df[map_columns.values()]
    return df


# %%
acte = "DBLF001"
version = "v2016"
version = "v2021"
geo = "reg"
geo = "dep"
df = format_table(acte, geo, version)
df


# %%
df_list = []
for params in iter_acte_geo_version():
    df_list.append(format_table(*params))
df = pd.concat(df_list)

# %%
np.testing.assert_array_equal(
    df.index.duplicated(),  # Doublons dans l'index
    np.array(
        df.reset_index().duplicated()
    ),  # Doublons sur toutes les colonnes (dont index)
)  # Vérifier que les lignes en doublon dans l'index ont des valeurs identiques sur les lignes

df = df[
    ~df.index.duplicated()
]  # Suppression des lignes France, en doublon depuis les tables departement et région

# %%
filepath = "../data/taux-recours-secure" if secure_data else "../data/taux-recours"
print(filepath)
df.to_csv(filepath + ".csv")
df.to_excel(filepath + ".xlsx")
df.to_parquet(filepath + ".parquet")

# %%
df

# %%
df.reset_index().dtypes

# %%
