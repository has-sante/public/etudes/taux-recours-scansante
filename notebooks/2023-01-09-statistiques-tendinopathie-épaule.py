# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.14.5
#   kernelspec:
#     display_name: scansante-acte
#     language: python
#     name: scansante-acte
# ---

# %%
import numpy as np
import pandas as pd
import plotly.express as px
from taux_recours_scansante.constants import (
    DIR2DATA,
    DIR2DOCS,
    DIR2DOCS_STATIC,
    ETUDES_DISPONIBLES,
    FILE_NAME_TAUX_RECOURS,
)
from taux_recours_scansante.statistics import (
    calcul_statistiques,
    coefficient_variation,
)

from taux_recours_scansante.utils import (
    LABEL_DEPARTEMENT,
    LABEL_REGION,
    LABEL_TAUX_SEJOURS,
)

idx = pd.IndexSlice

# %%
secure_data = False
nom_etude = "tendinopathie-epaule"
file_name = FILE_NAME_TAUX_RECOURS
if secure_data:
    file_name += "-secure"
dir2data = DIR2DATA / nom_etude / (file_name + ".parquet")
# %%
df = pd.read_parquet(dir2data)
df = df.drop(columns=["indice_national", "indice_regional"])
dir2img = DIR2DOCS_STATIC / "img" / nom_etude
dir2img.mkdir(exist_ok=True, parents=True)
# %% [markdown]
# # Statistiques d'ensemble

# %%
annee = 2021
print(f"Statistiques générales en {annee}")
calcul_statistiques(df=df, annee=annee)


# %%
annee = 2021
print(f"en filtrant sur les DROM pour Departement, Region et sejours 'localisés'")
calcul_statistiques(df=df, annee=annee, metropole=True)


# %% [markdown]
# **Note :**
# - `taux_sejour` est le taux de séjour pour chaque groupe d'actes, pour 100 000 habitants, standardisée par rapport à la population nationale
# - les statistiques représentées par département et par région sont : la moyenne, écart type et coefficient de variation du `taux de séjour`
# - les moyennes des départements/régions n'ont pas beaucoup de sens. Elles sont différentes de la moyenne nationale, car on ne pondère pas par la population de chaque unité géographique. Elles sont représentées, pour illustrer la problématique, identique sur les autres statistiques.
# - le coefficient de variation est calculée par la formule `ecart_type/moyenne`

# %%

# %% [markdown]
# # Variation dans le temps

# %%
df_france = df.xs("France", level="niveau_geo").reset_index()

# %% [markdown]
# Courbes totales

# %%
fig = px.line(
    df_france.groupby(["groupe_acte", "annee", "libelle_geo"])
    .nombre_sejour.sum()
    .reset_index(),
    x="annee",
    y="nombre_sejour",
    color="groupe_acte",
    category_orders={"groupe_acte": ETUDES_DISPONIBLES[nom_etude].keys()},
)

fig.show()

# %% [markdown]
# Superposition par acte, coloré par groupe d'acte

# %%
fig = px.area(
    df_france,
    x="annee",
    y="nombre_sejour",
    color="groupe_acte",
    line_group="code_acte",
)
fig.show()


# %% [markdown]
# Superposition par acte, en 2 figures par groupe d'acte

# %%
fig = px.area(
    df_france,
    x="annee",
    y="nombre_sejour",
    color="code_acte",
    facet_row="groupe_acte",
    height=600,
)
fig.show()

# %% [markdown]
# # Variation dans l'espace en 2021

# %%
annee = 2021
stats = calcul_statistiques(df=df, annee=annee)

# %%

niveau_geo = "Region"
df_geo = df.xs((niveau_geo, annee), level=["niveau_geo", "annee"]).reset_index()
df_geo["libelle_geo"] = df_geo["code_geo"] + " - " + df_geo["libelle_geo"]

fig = px.bar(
    df_geo,
    y="libelle_geo",
    x="taux_sejour",
    facet_col="groupe_acte",  # , color="code_acte"
)
fig.update_layout(
    yaxis={"categoryorder": "total ascending"},
    xaxis_title=LABEL_TAUX_SEJOURS,
    yaxis_title=LABEL_REGION,
)
for groupe_acte in list(df_geo["groupe_acte"].unique()):
    fig.add_vline(
        x=stats["France"]["taux_sejour"][groupe_acte],
        col=1,
        line_width=3,
        line_color="black",
        annotation_text="Moyenne nationale",
        annotation_position="bottom right",
    )
fig.show()

# %%
niveau_geo = "Departement"
df_geo = df.xs((niveau_geo, annee), level=["niveau_geo", "annee"]).reset_index()
df_geo["libelle_geo"] = df_geo["code_geo"] + " - " + df_geo["libelle_geo"]

fig = px.bar(
    df_geo.dropna(),
    y="libelle_geo",
    x="taux_sejour",
    facet_col="groupe_acte",
    height=1600,
)
fig.update_layout(
    yaxis={"categoryorder": "total ascending"},
    xaxis_title=LABEL_TAUX_SEJOURS,
    yaxis_title=LABEL_DEPARTEMENT,
)
for groupe_acte in list(df_geo["groupe_acte"].unique()):
    fig.add_vline(
        x=stats["France"]["taux_sejour"][groupe_acte],
        col=1,
        line_width=2,
        line_color="black",
        annotation_text="Moyenne nationale",
        annotation_position="bottom right",
    )
fig.show()

# %%
r = pd.concat(
    [
        stats["France"]["taux_sejour"] - stats[niveau_geo]["ecart_type"],
        stats["France"]["taux_sejour"],
        stats["France"]["taux_sejour"] + stats[niveau_geo]["ecart_type"],
    ],
    axis=1,
)
r.columns = ["taux - ecart", "taux", "taux + ecart"]


# %% [markdown]
# # Variation dans l'espace et le temps

# %%
niveau_geo = "Region"
df_geo = (
    df.loc[idx[:, :, [niveau_geo, "France"]]]
    .groupby(["groupe_acte", "annee", "libelle_geo"])
    .taux_sejour.sum()
    .reset_index()
)

fig = px.line(
    df_geo,
    x="annee",
    y="taux_sejour",
    color="libelle_geo",
    facet_row="groupe_acte",
    height=1000,
    category_orders={"groupe_acte": list(df_geo["groupe_acte"].unique())},
)

# edit colors
for d in fig["data"]:
    d["line"]["width"] = 1
    if d["name"] in ["BRETAGNE", "LA RÉUNION", "PROVENCE-ALPES-CÔTE D'AZUR"]:
        d["line"]["color"] = "blue"
    elif d["name"] in ["MARTINIQUE", "GUYANE", "MAYOTTE", "GUADELOUPE"]:
        d["line"]["color"] = "goldenrod"
    elif d["name"] == "France":
        d["line"]["color"] = "black"
        d["line"]["dash"] = "dash"
        d["line"]["width"] = 2
    else:
        d["line"]["color"] = "lightgrey"


fig.show()

# %%
niveau_geo = "Departement"
idx = pd.IndexSlice
df_geo = (
    df.loc[idx[:, :, [niveau_geo, "France"]]]
    .groupby(["groupe_acte", "annee", "code_geo", "libelle_geo"])
    .taux_sejour.sum()
    .reset_index()
)
# df_geo["libelle_geo"] = df_geo["code_geo"] + " - " + df_geo["libelle_geo"]

fig = px.line(
    df_geo,
    x="annee",
    y="taux_sejour",
    color="libelle_geo",
    facet_row="groupe_acte",
    height=1500,
    category_orders={"groupe_acte": [groupe_acte, "Chirurgie cardiaque"]},
)

# edit colors
for d in fig["data"]:
    d["line"]["width"] = 1
    if d["name"] in [
        "CREUSE",
        "ALPES-MARITIMES",
        "HAUTES-ALPES",
        "CÔTES-D ARMOR",
    ]:
        d["line"]["color"] = "blue"
    elif d["name"] in ["MARTINIQUE", "GUYANE", "MAYOTTE", "GUADELOUPE"]:
        d["line"]["color"] = "goldenrod"
    elif d["name"] == "France":
        d["line"]["color"] = "black"
        d["line"]["width"] = 3
    else:
        d["line"]["color"] = "lightgrey"


fig.show()

# %% [markdown]
# # Cartes
#
# **Attention !** : le rendu est très dépendant de l'échelle de couleur choisie
#

# %%
import json

# https://france-geojson.gregoiredavid.fr/
geojson_dict = dict()

with open(DIR2DATA / "departements.geojson") as f:
    geojson_dict["Departement"] = json.load(f)

with open(DIR2DATA / "regions.geojson") as f:
    geojson_dict["Region"] = json.load(f)

# %%
stats = calcul_statistiques(df=df, annee=annee, metropole=True)
# %%
niveau_geo = "Region"
groupe_acte = "Chirurgie de l'épaule"

moyenne = stats["France"]["taux_sejour"][groupe_acte]
ecart_type = stats[niveau_geo]["ecart_type"][groupe_acte]

df_geo = (
    df.xs(
        (niveau_geo, annee, groupe_acte),
        level=["niveau_geo", "annee", "groupe_acte"],
    )
    .groupby(["code_geo", "libelle_geo"])
    .taux_sejour.sum()
    .reset_index()
)
if niveau_geo == "Region":
    df_geo = (
        df_geo.set_index("code_geo")
        .drop(["01", "02", "03", "04", "06"], errors="ignore")
        .reset_index()
    )
df_geo["libelle_geo"] = df_geo["code_geo"] + " - " + df_geo["libelle_geo"]


fig = px.choropleth(
    df_geo,
    geojson=geojson_dict[niveau_geo],
    color="taux_sejour",
    locations="code_geo",
    hover_name="libelle_geo",
    featureidkey="properties.code",
    projection="mercator",
    color_continuous_scale="Viridis_r",
    range_color=(moyenne - ecart_type, moyenne + ecart_type),
    #    legend
)
fig.update_geos(fitbounds="locations", visible=False)
fig.update_layout(
    margin={"r": 0, "t": 0, "l": 0, "b": 0}, legend_title=LABEL_TAUX_SEJOURS
)
fig.write_image(
    dir2img / f"taux-séjour-{nom_etude}-{annee}-{niveau_geo}.png",
    width=1920,
    height=1080,
)
fig.show()

# %%
niveau_geo = "Departement"

moyenne = stats["France"]["taux_sejour"][groupe_acte]
ecart_type = stats[niveau_geo]["ecart_type"][groupe_acte]

df_geo = (
    df.xs(
        (niveau_geo, annee, groupe_acte),
        level=["niveau_geo", "annee", "groupe_acte"],
    )
    .groupby(["code_geo", "libelle_geo"])
    .taux_sejour.sum()
    .reset_index()
)
if niveau_geo == "Region":
    df_geo = (
        df_geo.set_index("code_geo").drop(["01", "02", "03", "04", "06"]).reset_index()
    )
df_geo["libelle_geo"] = df_geo["code_geo"] + " - " + df_geo["libelle_geo"]


fig = px.choropleth(
    df_geo,
    geojson=geojson_dict[niveau_geo],
    color="taux_sejour",
    locations="code_geo",
    featureidkey="properties.code",
    hover_name="libelle_geo",
    projection="mercator",
    color_continuous_scale="Viridis_r",
    range_color=(moyenne - ecart_type, moyenne + ecart_type),
    height=800,
)
fig.update_geos(fitbounds="locations", visible=False)
fig.update_layout(margin={"r": 0, "t": 0, "l": 0, "b": 0})
fig.write_image(
    dir2img / f"taux-séjour-{nom_etude}-{annee}-{niveau_geo}.png",
    width=1920,
    height=1080,
)
fig.show()
fig.write_image(
    dir2img / f"taux-séjour-{nom_etude}-{annee}-{niveau_geo}.png",
    width=1920,
    height=1080,
)
# %%
niveau_geo = "Departement"

stats = calcul_statistiques(df=df, annee=2021, metropole=True)
moyenne = stats["France"]["taux_sejour"][groupe_acte]
ecart_type = stats[niveau_geo]["ecart_type"][groupe_acte]

df_geo = (
    df.xs((niveau_geo, groupe_acte), level=["niveau_geo", "groupe_acte"])
    .groupby(["code_geo", "libelle_geo", "annee"])
    .taux_sejour.sum()
    .reset_index()
)
df_geo["libelle_geo"] = df_geo["code_geo"] + " - " + df_geo["libelle_geo"]


fig = px.choropleth(
    df_geo,
    geojson=geojson_dict[niveau_geo],
    color="taux_sejour",
    locations="code_geo",
    animation_frame="annee",
    hover_name="libelle_geo",
    featureidkey="properties.code",
    projection="mercator",
    color_continuous_scale="Viridis_r",
    range_color=(moyenne - ecart_type, moyenne + ecart_type),
    # range_color=(moyenne - ecart_type, moyenne + ecart_type),
    height=800,
)
fig.update_geos(fitbounds="locations", visible=False)
fig.update_layout(margin={"r": 0, "t": 0, "l": 0, "b": 0})
fig.show()

# %%

# %% [markdown]
# Test déplacement DROM proche de métropole
#
# https://www.icem7.fr/cartographie/un-fond-de-carte-france-par-commune-optimise-pour-le-web-et-lanalyse-statistique/
# -proj webmercator \
# -affine where="INSEE_COM.indexOf('971')==0" shift=6355000,3330000 scale=1.5 \
# -affine where="INSEE_COM.indexOf('972')==0" shift=6480000,3505000 scale=1.5 \
# -affine where="INSEE_COM.indexOf('973')==0" shift=5760000,4720000 scale=0.35 \
# -affine where="INSEE_COM.indexOf('974')==0" shift=-6170000,7560000 scale=1.5 \
# -affine where="INSEE_COM.indexOf('976')==0" shift=-4885000,6590000 scale=1.5

# %%
import geojson

# %%
c = geojson_dict["Departement"]["features"][0]["geometry"]["coordinates"]


# %%


# %%
def translation(x):
    return x[0], x[1] * 3 / 4


# %%
for s in geojson_dict["Region"]["features"]:
    if s["geometry"]["type"] == "Polygon":
        f = geojson.Polygon
    else:
        f = geojson.MultiPolygon

    new_shape = geojson.utils.map_tuples(translation, f(s["geometry"]["coordinates"]))
    s["geometry"]["coordinates"] = json.loads(geojson.dumps(new_shape, sort_keys=True))[
        "coordinates"
    ]
