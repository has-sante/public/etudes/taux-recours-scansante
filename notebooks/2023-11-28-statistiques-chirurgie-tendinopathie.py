# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.14.5
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# %%
# %reload_ext autoreload
# %autoreload 2
# %%
import numpy as np
import pandas as pd
import plotly.express as px
from taux_recours_scansante.cartography import (
    dessine_carte_recours,
    get_coordonees_geo,
)
from taux_recours_scansante.constants import (
    DIR2DATA,
    DIR2DOCS,
    DIR2DOCS_STATIC,
    ETUDES_DISPONIBLES,
    FILE_NAME_TAUX_RECOURS,
    LABEL_ANNEE,
    LABEL_CODE_ACTE,
    LABEL_FINESS,
    LABEL_FINESS_GEO,
    LABEL_GROUPE_ACTE,
    LABEL_NB_SEJ,
    LABEL_NOM_ES,
    LABEL_TX_SEJ,
    LABEL_DEPARTEMENT,
    LABEL_REGION,
    LABEL_TAUX_SEJOURS,
    LABEL_TAUX_SEJOURS_COURS,
    PLOTLY_SHOW_CONFIG,
)
from taux_recours_scansante.statistics import (
    calcul_statistiques,
    coefficient_variation,
)


idx = pd.IndexSlice

# %% [markdown]

# # Configuration de l'étude

# Spécifier le nom de l'étude: afin d'obtenir le rapport

# %%
# renseigner `secure_data=True` uniquement si les données ont été téléchargées sans enlever les cas rares
# (uniquement à partir de la plateforme atih ou par identification sur scansanté avec un compte autorisé).

params = {
    "nom_etude": "chirurgie-tendinopathie",
    "secure_data": False,
    "cartes": {
        "années": [
            2015,
            2022,
        ],  # Trace une carte statique pour chaque année et définit la période pour les cartes dynamiques.
        "niveau_geo": "Departement",
    },
}

if params["nom_etude"] not in ETUDES_DISPONIBLES.keys():
    raise ValueError(
        f"L'étude doit être parmi les études disponibles: {ETUDES_DISPONIBLES}"
    )

dir2data = DIR2DATA / params["nom_etude"] / (FILE_NAME_TAUX_RECOURS + ".parquet")
if params["secure_data"]:
    dir2data = (
        DIR2DATA / params["nom_etude"] / (FILE_NAME_TAUX_RECOURS + "-secure.parquet")
    )

# %%
df = pd.read_parquet(dir2data)
df = df.drop(columns=["indice_national", "indice_regional"])
dir2img = DIR2DOCS_STATIC / "img" / params["nom_etude"]
dir2img.mkdir(exist_ok=True, parents=True)
# %% [markdown]
# # Statistiques d'ensemble

# %%
print("Codes étudiés:\n")
print(df.index.unique(level=LABEL_CODE_ACTE))
# %%
annee = 2022
print(f"Statistiques générales en {annee}")
calcul_statistiques(df=df, annee=annee)

# %%
annee = 2022
print(f"en filtrant sur les DROM pour Departement, Region et sejours 'localisés'")
calcul_statistiques(df=df, annee=annee, metropole=True)


# %% **Note :** [markdown]
# - `taux_sejour` est le taux de séjour pour chaque groupe d'actes, pour 100 000
#   habitants, standardisé par rapport à la population nationale.
#
# - les statistiques représentées par département et par région sont : la
#   moyenne, écart type et coefficient de variation du `taux de séjour`.
#
# - les moyennes des départements/régions n'ont pas beaucoup de sens. Elles sont
#   différentes de la moyenne nationale, car on ne pondère pas par la population
#   de chaque unité géographique. Elles sont représentées, pour illustrer la
#   problématique, identique sur les autres statistiques.
#
# - le coefficient de variation est calculé par la formule
#   [`ecart_type/moyenne`](https://fr.wikipedia.org/wiki/Coefficient_de_variation).

# %% [markdown]
# # Variation dans le temps

# %%
df_france = df.xs("France", level="niveau_geo").reset_index()

# %% [markdown]
# Courbes totales

# %%
fig = px.line(
    df_france.groupby([LABEL_GROUPE_ACTE, LABEL_ANNEE, "libelle_geo"])[LABEL_NB_SEJ]
    .sum()
    .reset_index(),
    x=LABEL_ANNEE,
    y=LABEL_NB_SEJ,
    color=LABEL_GROUPE_ACTE,
    category_orders={LABEL_GROUPE_ACTE: ETUDES_DISPONIBLES[params["nom_etude"]].keys()},
)
fig.update_layout(xaxis=dict(tickmode="linear"))
fig.show(config=PLOTLY_SHOW_CONFIG)
# %% [markdown]
# Superposition par acte, coloré par groupe d'actes

# %%
fig = px.area(
    df_france,
    x=LABEL_ANNEE,
    y=LABEL_NB_SEJ,
    color=LABEL_GROUPE_ACTE,
    line_group=LABEL_CODE_ACTE,
)
fig.update_layout(xaxis=dict(tickmode="linear"))
fig.show(config=PLOTLY_SHOW_CONFIG)
# %% [markdown]
# Superposition par acte, en deux figures par groupe d'actes

# %%
fig = px.area(
    df_france,
    x=LABEL_ANNEE,
    y=LABEL_NB_SEJ,
    color=LABEL_CODE_ACTE,
    facet_row=LABEL_GROUPE_ACTE,
    height=600,
)
fig.update_layout(xaxis=dict(tickmode="linear"))
config = PLOTLY_SHOW_CONFIG.copy()
config["toImageButtonOptions"]["height"] = (
    config["toImageButtonOptions"]["height"] * 1.5
)
fig.show(config=PLOTLY_SHOW_CONFIG)

# %% [markdown]
# # Variation dans l'espace en 2022

# %%
annee = 2022
stats = calcul_statistiques(df=df, annee=annee)

# %%
niveau_geo = "Region"
for groupe_acte in df.index.get_level_values(LABEL_GROUPE_ACTE).unique():
    df_geo_groupe_actes = df.xs(
        (niveau_geo, annee, groupe_acte),
        level=["niveau_geo", LABEL_ANNEE, LABEL_GROUPE_ACTE],
    ).reset_index()
    df_geo_groupe_actes["libelle_geo"] = df_geo_groupe_actes["libelle_geo"]
    df_geo_groupe_actes = df_geo_groupe_actes.loc[
        ~df_geo_groupe_actes[LABEL_NB_SEJ].isna()
    ]
    fig = px.bar(
        df_geo_groupe_actes,
        y="libelle_geo",
        x=LABEL_TX_SEJ,
        hover_data=[LABEL_CODE_ACTE],
        # facet_col=LABEL_GROUPE_ACTE,  # , color=LABEL_CODE_ACTE
    )
    fig.add_vline(
        x=stats["France"][LABEL_TX_SEJ][groupe_acte],
        # col=i,
        line_width=3,
        line_color="black",
        annotation_text="Moyenne nationale",
        annotation_position="bottom right",
    )
    fig.update_layout(
        yaxis={"categoryorder": "total ascending"},
        xaxis_title=LABEL_TAUX_SEJOURS,
        yaxis_title=LABEL_REGION,
        title=f"Actes: {groupe_acte}",
    )
    fig.show(config=PLOTLY_SHOW_CONFIG)

# %%
niveau_geo = "Departement"
for groupe_acte in df.index.get_level_values(LABEL_GROUPE_ACTE).unique():
    df_geo_groupe_actes = df.xs(
        (niveau_geo, annee, groupe_acte),
        level=["niveau_geo", LABEL_ANNEE, LABEL_GROUPE_ACTE],
    ).reset_index()
    df_geo_groupe_actes["libelle_geo"] = df_geo_groupe_actes["libelle_geo"]
    df_geo_groupe_actes = df_geo_groupe_actes.loc[
        ~df_geo_groupe_actes[LABEL_NB_SEJ].isna()
    ]
    fig = px.bar(
        df_geo_groupe_actes,
        y="libelle_geo",
        x=LABEL_TX_SEJ,
        # facet_col=LABEL_GROUPE_ACTE,  # , color=LABEL_CODE_ACTE
        height=1600,
    )
    fig.add_vline(
        x=stats["France"][LABEL_TX_SEJ][groupe_acte],
        # col=i,
        line_width=3,
        line_color="black",
        annotation_text="Moyenne nationale",
        annotation_position="bottom right",
    )
    fig.update_layout(
        yaxis={"categoryorder": "total ascending"},
        xaxis_title=LABEL_TAUX_SEJOURS,
        yaxis_title=LABEL_DEPARTEMENT,
        title=f"Actes: {groupe_acte}",
    )
    fig.show()
# %%
r = pd.concat(
    [
        stats["France"][LABEL_TX_SEJ] - stats[niveau_geo]["ecart_type"],
        stats["France"][LABEL_TX_SEJ],
        stats["France"][LABEL_TX_SEJ] + stats[niveau_geo]["ecart_type"],
    ],
    axis=1,
)
r.columns = ["taux - ecart", "taux", "taux + ecart"]


# %% [markdown]
# # Variation dans l'espace et le temps

# %%
niveau_geo = "Region"
df_geo_groupe_actes = (
    df.loc[idx[:, :, [niveau_geo, "France"]]]
    .groupby([LABEL_GROUPE_ACTE, LABEL_ANNEE, "libelle_geo"])[LABEL_TX_SEJ]
    .sum()
    .reset_index()
    .rename(columns={"libelle_geo": "Région"})
)
# chat gpt palette
region_colors = [
    "#1f77b4",
    "#ff7f0e",
    "#2ca02c",
    "#d62728",
    "#9467bd",
    "#8c564b",
    "#e377c2",
    "#7f7f7f",
    "#bcbd22",
    "#17becf",
    "#ffbb78",
    "#98df8a",
    "#ff9896",
    "#c5b0d5",
    "#c49c94",
    "#f7b6d2",
    "#c7c7c7",
    "#dbdb8d",
]
fig = px.line(
    df_geo_groupe_actes,
    x=LABEL_ANNEE,
    y=LABEL_TX_SEJ,
    color="Région",
    color_discrete_sequence=region_colors,  # px.colors.qualitative.Alphabet,
    facet_row=LABEL_GROUPE_ACTE,
    height=1000,
    category_orders={
        LABEL_GROUPE_ACTE: list(df_geo_groupe_actes[LABEL_GROUPE_ACTE].unique())
    },
)
fig.update_layout(xaxis=dict(tickmode="linear"))
fig.show(config=PLOTLY_SHOW_CONFIG)

# %%
niveau_geo = "Departement"
idx = pd.IndexSlice
df_geo_groupe_actes = (
    df.loc[idx[:, :, [niveau_geo, "France"]]]
    .groupby([LABEL_GROUPE_ACTE, LABEL_ANNEE, "code_geo", "libelle_geo"])[LABEL_TX_SEJ]
    .sum()
    .reset_index()
    .rename(columns={"libelle_geo": "Département"})
)
# df_geo["libelle_geo"] = df_geo["code_geo"] + " - " + df_geo["libelle_geo"]

fig = px.line(
    df_geo_groupe_actes,
    x=LABEL_ANNEE,
    y=LABEL_TX_SEJ,
    color="Département",
    facet_row=LABEL_GROUPE_ACTE,
    height=1500,
    category_orders={LABEL_GROUPE_ACTE: [groupe_acte, "Chirurgie cardiaque"]},
)
fig.update_layout(xaxis=dict(tickmode="linear"))

# edit colors
for d in fig["data"]:
    d["line"]["width"] = 1
    if d["name"] in [
        "CREUSE",
        "ALPES-MARITIMES",
        "HAUTES-ALPES",
        "CÔTES-D ARMOR",
    ]:
        d["line"]["color"] = "blue"
    elif d["name"] in ["MARTINIQUE", "GUYANE", "MAYOTTE", "GUADELOUPE"]:
        d["line"]["color"] = "goldenrod"
    elif d["name"] == "France":
        d["line"]["color"] = "black"
        d["line"]["width"] = 3
    else:
        d["line"]["color"] = "lightgrey"
fig.show(config=PLOTLY_SHOW_CONFIG)
# %% [markdown]
# # Cartes
#
# **Attention !** : le rendu est très dépendant de l'échelle de couleur choisie
#

# %%
import json

# https://france-geojson.gregoiredavid.fr/
geojson_dict = dict()

with open(DIR2DATA / "departements.geojson") as f:
    geojson_dict["Departement"] = json.load(f)


with open(DIR2DATA / "regions.geojson") as f:
    geojson_dict["Region"] = json.load(f)

# %%
annees_cartes_statiques = params["cartes"]["années"]

for annee in annees_cartes_statiques:
    for groupe_acte in [
        None,
        *list(df.index.get_level_values(LABEL_GROUPE_ACTE).unique()),
    ]:
        if not groupe_acte:
            groupe_acte_lib = "Tout abord"
        else:
            groupe_acte_lib = f"Abord {groupe_acte}"
        print(f"{groupe_acte_lib} pour l'année {annee}")
        fig = dessine_carte_recours(
            df=df,
            annee=annee,
            groupe_acte=groupe_acte,
            niveau_geo=params["cartes"]["niveau_geo"],
        )
        fig.show(config=PLOTLY_SHOW_CONFIG)


# %% [markdown]
# # Cartes de taux de recours du TAVI année par année
# %%
niveau_geo = params["cartes"]["niveau_geo"]
for groupe_acte in [
    None,
    *list(df.index.get_level_values(LABEL_GROUPE_ACTE).unique()),
]:
    if not groupe_acte:
        groupe_acte_lib = "Tout abord"
    else:
        groupe_acte_lib = f"Abord {groupe_acte}"
    print(f"{groupe_acte_lib} pour les années {params['cartes']['années']}")
    if groupe_acte is not None:
        stats_all_year = (
            df.xs(groupe_acte, level=LABEL_GROUPE_ACTE)
            .reset_index()
            .groupby([LABEL_ANNEE, "libelle_geo"])[LABEL_TX_SEJ]
            .agg("sum")
        )
        df_geo = df.xs((niveau_geo), level="niveau_geo")
        df_geo_groupe_actes = (
            df.xs((groupe_acte), level=LABEL_GROUPE_ACTE)
            .groupby(["code_geo", "libelle_geo", LABEL_ANNEE])[LABEL_TX_SEJ]
            .sum()
            .reset_index()
        )
    else:
        stats_all_year = (
            df.droplevel(LABEL_GROUPE_ACTE)
            .reset_index()
            .groupby([LABEL_ANNEE, "libelle_geo"])[LABEL_TX_SEJ]
            .agg("sum")
        )
        df_geo_groupe_actes = (
            df.droplevel(LABEL_GROUPE_ACTE)
            .xs(niveau_geo, level="niveau_geo")
            .groupby(["code_geo", "libelle_geo", LABEL_ANNEE])[LABEL_TX_SEJ]
            .sum()
            .reset_index()
        )
    moyenne = stats_all_year.mean()
    ecart_type = stats_all_year.std()

    df_geo_groupe_actes["libelle_geo"] = (
        df_geo_groupe_actes["code_geo"] + " - " + df_geo_groupe_actes["libelle_geo"]
    )

    fig = px.choropleth(
        df_geo_groupe_actes,
        geojson=geojson_dict[niveau_geo],
        color=LABEL_TX_SEJ,
        locations="code_geo",
        animation_frame=LABEL_ANNEE,
        hover_name="libelle_geo",
        featureidkey="properties.code",
        projection="mercator",
        color_continuous_scale="Viridis_r",
        range_color=(moyenne - 2 * ecart_type, moyenne + 2 * ecart_type),
        # range_color=(moyenne - ecart_type, moyenne + ecart_type),
        height=800,
    )

    fig.update_geos(fitbounds="locations", visible=False)
    tickvals = [
        moyenne - 2 * ecart_type,
        moyenne - ecart_type,
        moyenne,
        moyenne + ecart_type,
        moyenne + 2 * ecart_type,
    ]
    fig.update_layout(
        margin={"r": 0, "t": 0, "l": 0, "b": 0},
        coloraxis_colorbar=dict(
            title=LABEL_TAUX_SEJOURS_COURS,
            tickvals=tickvals,
            ticktext=[
                f"{t:.1f}" if t != moyenne else f"Moyenne: {t:.1f} " for t in tickvals
            ],
        ),
    )
    fig.show(config=PLOTLY_SHOW_CONFIG)
# %%
