# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.14.5
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# %%
# %load_ext autoreload
# %autoreload 2
# %%
import numpy as np
import pandas as pd
import plotly.express as px
from taux_recours_scansante.cartography import (
    dessine_carte_recours,
    get_coordonees_geo,
)
from taux_recours_scansante.constants import (
    DIR2DATA,
    DIR2DOCS,
    DIR2DOCS_STATIC,
    ETUDES_DISPONIBLES,
    FILE_NAME_TAUX_RECOURS,
    LABEL_ANNEE,
    LABEL_CODE_ACTE,
    LABEL_FINESS,
    LABEL_FINESS_GEO,
    LABEL_GROUPE_ACTE,
    LABEL_NB_SEJ,
    LABEL_NOM_ES,
    LABEL_TX_SEJ,
    LABEL_DEPARTEMENT,
    LABEL_REGION,
    LABEL_TAUX_SEJOURS,
    LABEL_TAUX_SEJOURS_COURS,
    PLOTLY_SHOW_CONFIG,
)
from taux_recours_scansante.statistics import (
    calcul_statistiques,
    coefficient_variation,
)


idx = pd.IndexSlice

# %%
secure_data = False
nom_etude = "tavi"
file_name = FILE_NAME_TAUX_RECOURS
if secure_data:
    file_name += "-secure"
dir2data = DIR2DATA / nom_etude / (file_name + ".parquet")
# %%
df = pd.read_parquet(dir2data)
df = df.drop(columns=["indice_national", "indice_regional"])
dir2img = DIR2DOCS_STATIC / "img" / nom_etude
dir2img.mkdir(exist_ok=True, parents=True)
# %% [markdown]
# # Statistiques d'ensemble

# %%
print("Codes étudiés:\n")
print(df.index.unique(level=LABEL_CODE_ACTE))
# %%
annee = 2022
print(f"Statistiques générales en {annee}")
calcul_statistiques(df=df, annee=annee)

# %%
annee = 2022
print(f"en filtrant sur les DROM pour Departement, Region et sejours 'localisés'")
calcul_statistiques(df=df, annee=annee, metropole=True)


# %% **Note :** [markdown]
# - `taux_sejour` est le taux de séjour pour chaque groupe d'actes, pour 100 000
#   habitants, standardisé par rapport à la population nationale.
#
# - les statistiques représentées par département et par région sont : la
#   moyenne, écart type et coefficient de variation du `taux de séjour`.
#
# - les moyennes des départements/régions n'ont pas beaucoup de sens. Elles sont
#   différentes de la moyenne nationale, car on ne pondère pas par la population
#   de chaque unité géographique. Elles sont représentées, pour illustrer la
#   problématique, identique sur les autres statistiques.
#
# - le coefficient de variation est calculé par la formule
#   [`ecart_type/moyenne`](https://fr.wikipedia.org/wiki/Coefficient_de_variation).

# %% [markdown]
# # Variation dans le temps

# %%
df_france = df.xs("France", level="niveau_geo").reset_index()

# %% [markdown]
# Courbes totales

# %%
fig = px.line(
    df_france.groupby([LABEL_GROUPE_ACTE, LABEL_ANNEE, "libelle_geo"])[LABEL_NB_SEJ]
    .sum()
    .reset_index(),
    x=LABEL_ANNEE,
    y=LABEL_NB_SEJ,
    color=LABEL_GROUPE_ACTE,
    category_orders={LABEL_GROUPE_ACTE: ETUDES_DISPONIBLES[nom_etude].keys()},
)
fig.update_layout(xaxis=dict(tickmode="linear"))
fig.show(config=PLOTLY_SHOW_CONFIG)
# %% [markdown]
# Superposition par acte, coloré par groupe d'actes

# %%
fig = px.area(
    df_france,
    x=LABEL_ANNEE,
    y=LABEL_NB_SEJ,
    color=LABEL_GROUPE_ACTE,
    line_group=LABEL_CODE_ACTE,
)
fig.update_layout(xaxis=dict(tickmode="linear"))
fig.show(config=PLOTLY_SHOW_CONFIG)
# %% [markdown]
# Superposition par acte, en deux figures par groupe d'actes

# %%
fig = px.area(
    df_france,
    x=LABEL_ANNEE,
    y=LABEL_NB_SEJ,
    color=LABEL_CODE_ACTE,
    facet_row=LABEL_GROUPE_ACTE,
    height=600,
)
fig.update_layout(xaxis=dict(tickmode="linear"))
config = PLOTLY_SHOW_CONFIG.copy()
config["toImageButtonOptions"]["height"] = (
    config["toImageButtonOptions"]["height"] * 1.5
)
fig.show(config=PLOTLY_SHOW_CONFIG)

# %% [markdown]
# # Variation dans l'espace en 2022

# %%
annee = 2022
stats = calcul_statistiques(df=df, annee=annee)

# %%
niveau_geo = "Region"
for groupe_acte in df.index.get_level_values(LABEL_GROUPE_ACTE).unique():
    df_geo_groupe_actes = df.xs(
        (niveau_geo, annee, groupe_acte),
        level=["niveau_geo", LABEL_ANNEE, LABEL_GROUPE_ACTE],
    ).reset_index()
    df_geo_groupe_actes["libelle_geo"] = df_geo_groupe_actes["libelle_geo"]
    df_geo_groupe_actes = df_geo_groupe_actes.loc[
        ~df_geo_groupe_actes[LABEL_NB_SEJ].isna()
    ]
    fig = px.bar(
        df_geo_groupe_actes,
        y="libelle_geo",
        x=LABEL_TX_SEJ,
        hover_data=[LABEL_CODE_ACTE],
        # facet_col=LABEL_GROUPE_ACTE,  # , color=LABEL_CODE_ACTE
    )
    fig.add_vline(
        x=stats["France"][LABEL_TX_SEJ][groupe_acte],
        # col=i,
        line_width=3,
        line_color="black",
        annotation_text="Moyenne nationale",
        annotation_position="bottom right",
    )
    fig.update_layout(
        yaxis={"categoryorder": "total ascending"},
        xaxis_title=LABEL_TAUX_SEJOURS,
        yaxis_title=LABEL_REGION,
        title=f"Actes: {groupe_acte}",
    )
    fig.show(config=PLOTLY_SHOW_CONFIG)

# %%
niveau_geo = "Departement"
for groupe_acte in df.index.get_level_values(LABEL_GROUPE_ACTE).unique():
    df_geo_groupe_actes = df.xs(
        (niveau_geo, annee, groupe_acte),
        level=["niveau_geo", LABEL_ANNEE, LABEL_GROUPE_ACTE],
    ).reset_index()
    df_geo_groupe_actes["libelle_geo"] = df_geo_groupe_actes["libelle_geo"]
    df_geo_groupe_actes = df_geo_groupe_actes.loc[
        ~df_geo_groupe_actes[LABEL_NB_SEJ].isna()
    ]
    fig = px.bar(
        df_geo_groupe_actes,
        y="libelle_geo",
        x=LABEL_TX_SEJ,
        # facet_col=LABEL_GROUPE_ACTE,  # , color=LABEL_CODE_ACTE
        height=1600,
    )
    fig.add_vline(
        x=stats["France"][LABEL_TX_SEJ][groupe_acte],
        # col=i,
        line_width=3,
        line_color="black",
        annotation_text="Moyenne nationale",
        annotation_position="bottom right",
    )
    fig.update_layout(
        yaxis={"categoryorder": "total ascending"},
        xaxis_title=LABEL_TAUX_SEJOURS,
        yaxis_title=LABEL_DEPARTEMENT,
        title=f"Actes: {groupe_acte}",
    )
    fig.show()
# %%
r = pd.concat(
    [
        stats["France"][LABEL_TX_SEJ] - stats[niveau_geo]["ecart_type"],
        stats["France"][LABEL_TX_SEJ],
        stats["France"][LABEL_TX_SEJ] + stats[niveau_geo]["ecart_type"],
    ],
    axis=1,
)
r.columns = ["taux - ecart", "taux", "taux + ecart"]


# %% [markdown]
# # Variation dans l'espace et le temps

# %%
niveau_geo = "Region"
df_geo_groupe_actes = (
    df.loc[idx[:, :, [niveau_geo, "France"]]]
    .groupby([LABEL_GROUPE_ACTE, LABEL_ANNEE, "libelle_geo"])[LABEL_TX_SEJ]
    .sum()
    .reset_index()
    .rename(columns={"libelle_geo": "Région"})
)
# chat gpt palette
region_colors = [
    "#1f77b4",
    "#ff7f0e",
    "#2ca02c",
    "#d62728",
    "#9467bd",
    "#8c564b",
    "#e377c2",
    "#7f7f7f",
    "#bcbd22",
    "#17becf",
    "#ffbb78",
    "#98df8a",
    "#ff9896",
    "#c5b0d5",
    "#c49c94",
    "#f7b6d2",
    "#c7c7c7",
    "#dbdb8d",
]
fig = px.line(
    df_geo_groupe_actes,
    x=LABEL_ANNEE,
    y=LABEL_TX_SEJ,
    color="Région",
    color_discrete_sequence=region_colors,  # px.colors.qualitative.Alphabet,
    facet_row=LABEL_GROUPE_ACTE,
    height=1000,
    category_orders={
        LABEL_GROUPE_ACTE: list(df_geo_groupe_actes[LABEL_GROUPE_ACTE].unique())
    },
)
fig.update_layout(xaxis=dict(tickmode="linear"))
fig.show(config=PLOTLY_SHOW_CONFIG)

# %%
niveau_geo = "Departement"
idx = pd.IndexSlice
df_geo_groupe_actes = (
    df.loc[idx[:, :, [niveau_geo, "France"]]]
    .groupby([LABEL_GROUPE_ACTE, LABEL_ANNEE, "code_geo", "libelle_geo"])[LABEL_TX_SEJ]
    .sum()
    .reset_index()
    .rename(columns={"libelle_geo": "Département"})
)
# df_geo["libelle_geo"] = df_geo["code_geo"] + " - " + df_geo["libelle_geo"]

fig = px.line(
    df_geo_groupe_actes,
    x=LABEL_ANNEE,
    y=LABEL_TX_SEJ,
    color="Département",
    facet_row=LABEL_GROUPE_ACTE,
    height=1500,
    category_orders={LABEL_GROUPE_ACTE: [groupe_acte, "Chirurgie cardiaque"]},
)
fig.update_layout(xaxis=dict(tickmode="linear"))

# edit colors
for d in fig["data"]:
    d["line"]["width"] = 1
    if d["name"] in [
        "CREUSE",
        "ALPES-MARITIMES",
        "HAUTES-ALPES",
        "CÔTES-D ARMOR",
    ]:
        d["line"]["color"] = "blue"
    elif d["name"] in ["MARTINIQUE", "GUYANE", "MAYOTTE", "GUADELOUPE"]:
        d["line"]["color"] = "goldenrod"
    elif d["name"] == "France":
        d["line"]["color"] = "black"
        d["line"]["width"] = 3
    else:
        d["line"]["color"] = "lightgrey"
fig.show(config=PLOTLY_SHOW_CONFIG)
# %% [markdown]
# # Cartes
#
# **Attention !** : le rendu est très dépendant de l'échelle de couleur choisie
#

# %%
import json

# https://france-geojson.gregoiredavid.fr/
geojson_dict = dict()

with open(DIR2DATA / "departements.geojson") as f:
    geojson_dict["Departement"] = json.load(f)

with open(DIR2DATA / "regions.geojson") as f:
    geojson_dict["Region"] = json.load(f)
# %%
dir2activite_centres = DIR2DATA / "activité_centres_tavi.xlsx"

# J'utilise le finess download depuis scope santé:
path2finess = DIR2DATA / "finess_scope_sante.csv"
if not path2finess.exists():
    print(
        "Downloading finess from scope santé: https://www.data.gouv.fr/fr/datasets/base-sur-la-qualite-et-la-securite-des-soins-anciennement-scope-sante/"
    )
    finess = pd.read_csv(
        "https://www.data.gouv.fr/fr/datasets/r/48dadbce-56a8-4dc4-ac51-befcdfd82521"
    )
    finess.to_csv(path2finess, index=False)
finess_df = (
    pd.read_csv(DIR2DATA / "finess_scope_sante.csv")
    .sort_values("date_export")
    .groupby("num_finess_et")
    .last()
    .reset_index()
)
activite_centres_tavi = pd.read_excel(
    dir2activite_centres, sheet_name="TAVI", skiprows=5, skipfooter=1
)
activite_centres_tavi_geoloc = get_coordonees_geo(
    finess_df=finess_df, activite_centres=activite_centres_tavi
)
marker_prop = 700
# %%
annee = 2022
groupe_acte = "TAVI"
niveau_geo = "Region"

activite_centres_tavi_geoloc["marker_size"] = (
    marker_prop
    * activite_centres_tavi_geoloc[str(annee)]
    / activite_centres_tavi_geoloc[str(annee)].sum()
)
activite_centres_tavi_geoloc["label"] = (
    activite_centres_tavi_geoloc.raison_sociale_et
    + "<br> Nb TAVI (2015-2022): "
    + activite_centres_tavi_geoloc[str(annee)].astype(str)
)

activite_centres_tavi_geoloc_annee = activite_centres_tavi_geoloc.loc[
    ~activite_centres_tavi_geoloc["marker_size"].isna()
]
fig = dessine_carte_recours(
    df=df,
    annee=annee,
    groupe_acte=groupe_acte,
    niveau_geo=niveau_geo,
)
fig.add_scattergeo(
    lat=activite_centres_tavi_geoloc_annee.latitude,
    lon=activite_centres_tavi_geoloc_annee.longitude,
    text=activite_centres_tavi_geoloc_annee.label,
    mode="markers",
    showlegend=False,
    name="Centre TAVI",
    # legendgroup="Centre TAVI: activité",
    marker=dict(
        size=activite_centres_tavi_geoloc_annee.marker_size,
        color="lightcoral",
        # line=dict(width=1,color='DarkSlateGrey')
    ),
)
# strange error at saving time from kalideo: might be HAS proxy
# fig.write_image(
#     file=dir2img / f"taux-séjour-{nom_etude}-{annee}-{niveau_geo}.png",
#     width=1920,
#     height=1080,
# )

# Add markersize legend : Timone peuchère
legend_row = activite_centres_tavi_geoloc_annee.loc[
    activite_centres_tavi_geoloc_annee["num_finess_et"] == "130783293"
]
marker_size_legend = legend_row["marker_size"].values[0]
marker_count_tavi = int(legend_row[str(annee)].values[0])
fig.add_scattergeo(
    lat=[50],
    lon=[-3.5],
    text=[f"Centres TAVI<br>({marker_count_tavi} actes en {annee})"],
    textposition="top center",
    mode="markers+text",
    name=f"Centre Tavi",
    showlegend=False,
    marker=dict(size=marker_size_legend, color="lightcoral"),
)
fig.show(config=PLOTLY_SHOW_CONFIG)
# %%
annee = 2022
groupe_acte = "TAVI"
niveau_geo = "Departement"
fig = dessine_carte_recours(
    df=df,
    annee=annee,
    groupe_acte=groupe_acte,
    niveau_geo=niveau_geo,
)
fig.add_scattergeo(
    lat=activite_centres_tavi_geoloc_annee.latitude,
    lon=activite_centres_tavi_geoloc_annee.longitude,
    text=activite_centres_tavi_geoloc_annee.label,
    mode="markers",
    showlegend=False,
    marker=dict(
        size=activite_centres_tavi_geoloc_annee.marker_size,
        color="lightcoral",
    ),
)
fig.add_scattergeo(
    lat=[50],
    lon=[-3.5],
    text=[f"Centres TAVI<br>({marker_count_tavi} actes en {annee})"],
    textposition="top center",
    mode="markers+text",
    name=f"Centre Tavi",
    showlegend=False,
    marker=dict(size=marker_size_legend, color="lightcoral"),
)

fig.show(config=PLOTLY_SHOW_CONFIG)

# %%
activite_centres_tavi_geoloc_melted = activite_centres_tavi_geoloc.melt(
    id_vars=["latitude", "longitude", "label"],
    value_vars=[
        "2015",
        "2016",
        "2017",
        "2018",
        "2019",
        "2020",
        "2021",
        "2022",
    ],
    value_name="nb_tavi",
    var_name=LABEL_ANNEE,
)
activite_centres_tavi_geoloc_melted["nb_tavi_normalized"] = (
    10
    * marker_prop
    * activite_centres_tavi_geoloc_melted["nb_tavi"]
    / activite_centres_tavi_geoloc["nb_tavi_total"].sum()
)
activite_centres_tavi_geoloc_melted = activite_centres_tavi_geoloc_melted[
    ~activite_centres_tavi_geoloc_melted["nb_tavi_normalized"].isna()
]

# %% [markdown]
# # Cartes de taux de recours du TAVI année par année
# %%
niveau_geo = "Departement"
groupe_acte = "TAVI"

stats_all_year = (
    df.xs(groupe_acte, level=LABEL_GROUPE_ACTE)
    .reset_index()
    .groupby([LABEL_ANNEE, "libelle_geo"])[LABEL_TX_SEJ]
    .agg("sum")
)
moyenne = stats_all_year.mean()
ecart_type = stats_all_year.std()

df_geo_groupe_actes = (
    df.xs((niveau_geo, groupe_acte), level=["niveau_geo", LABEL_GROUPE_ACTE])
    .groupby(["code_geo", "libelle_geo", LABEL_ANNEE])[LABEL_TX_SEJ]
    .sum()
    .reset_index()
)
df_geo_groupe_actes["libelle_geo"] = (
    df_geo_groupe_actes["code_geo"] + " - " + df_geo_groupe_actes["libelle_geo"]
)


fig = px.choropleth(
    df_geo_groupe_actes,
    geojson=geojson_dict[niveau_geo],
    color=LABEL_TX_SEJ,
    locations="code_geo",
    animation_frame=LABEL_ANNEE,
    hover_name="libelle_geo",
    featureidkey="properties.code",
    projection="mercator",
    color_continuous_scale="Viridis_r",
    range_color=(moyenne - 2 * ecart_type, moyenne + 2 * ecart_type),
    # range_color=(moyenne - ecart_type, moyenne + ecart_type),
    height=800,
)

# more difficult than it seems
fig.add_scattergeo(
    # animation_frame=activite_centres_tavi_geoloc_melted[LABEL_ANNEE],
    lat=activite_centres_tavi_geoloc.latitude,
    lon=activite_centres_tavi_geoloc.longitude,
    marker=dict(size=10, color="lightcoral", line={"width": 1}),
)
fig.update_geos(fitbounds="locations", visible=False)
tickvals = [
    moyenne - 2 * ecart_type,
    moyenne - ecart_type,
    moyenne,
    moyenne + ecart_type,
    moyenne + 2 * ecart_type,
]
fig.update_layout(
    margin={"r": 0, "t": 0, "l": 0, "b": 0},
    coloraxis_colorbar=dict(
        title=LABEL_TAUX_SEJOURS_COURS,
        tickvals=tickvals,
        ticktext=[
            f"{t:.1f}" if t != moyenne else f"Moyenne: {t:.1f} " for t in tickvals
        ],
    ),
)
fig
# %%
