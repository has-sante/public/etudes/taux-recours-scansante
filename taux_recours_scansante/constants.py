import os
from pathlib import Path

ROOT_DIR = Path(
    os.getenv(
        "ROOT_DIR", Path(os.path.dirname(os.path.abspath(__file__))) / ".."
    )
)

DIR2DOCS = ROOT_DIR / "docs/source"
DIR2DOCS_STATIC = DIR2DOCS / "_static"


# Default paths
# Data
DIR2DATA = ROOT_DIR / "data"
DIR2OPEN_CCAM = DIR2DATA / "open_ccam"


ACTES_TAVI = {
    "TAVI": ["DBLF001", "DBLA004"],
    "Chirurgie cardiaque": [
        "DGKA011",
        "DGKA014",
        "DGKA015",
        "DGKA018",
        "DBKA001",
        "DBKA006",
        "DBKA003",
        "DBKA011",
        "DBKA009",
        "DBMA001",
        "DBMA006",
        "DBMA009",
        "DBMA010",
        "DBMA015",
    ],
}

ACTES_CHIRURGIE_EPAULE = {
    "Chirurgie de l'épaule": [
        "MEMA006",  # Acromioplastie sans prothèse, par abord direct
        "MEMA011",  # Arthroplastie acromioclaviculaire par résection de l'extrémité latérale de la clavicule, par arthrotomie
        "MEMA017",  # Acromioplastie sans prothèse avec arthroplastie acromioclaviculaire par résection de l'extrémité latérale de la clavicule, par abord direct
        "MEMC001",  # Arthroplastie acromioclaviculaire par résection de l'extrémité latérale de la clavicule, par arthroscopie
        "MEMC003",  # Acromioplastie sans prothèse, par arthroscopie
        "MEMC005",  # Acromioplastie sans prothèse avec arthroplastie acromioclaviculaire par résection de l'extrémité latérale de la clavicule, par arthroscopie
    ]
}

ACTES_CHIRURGIE_TENDINOPATHIE = {
    "NA": [
        "MJMA003",  # Réparation de la coiffe des rotateurs de l'épaule par autoplastie et/ou matériel prothétique, par abord direct
    ],
    "Ciel ouvert": [
        "MJEA006",  # Réinsertion et/ou suture de plusieurs tendons de la coiffe des rotateurs de l'épaule, par abord direct
        "MJEA010",  # Réinsertion ou suture d'un tendon de la coiffe des rotateurs de l'épaule, par abord direct
    ],
    "Arthroscopie": [
        "MJEC002",  # Réinsertion et/ou suture de plusieurs tendons de la coiffe des rotateurs de l'épaule, par arthroscopie
        "MJEC001",  # Réinsertion ou suture d'un tendon de la coiffe des rotateurs de l'épaule, par arthroscopie
    ],
}


ETUDES_DISPONIBLES = {
    "tavi": ACTES_TAVI,
    "tendinopathie-epaule": ACTES_CHIRURGIE_EPAULE,
    "chirurgie-tendinopathie": ACTES_CHIRURGIE_TENDINOPATHIE,
}

FILE_NAME_TAUX_RECOURS = "taux-recours"

# Constantes rapport

LABEL_CODE_ACTE = "Code acte"
LABEL_GROUPE_ACTE = "Groupe d'actes"
LABEL_NB_SEJ = "Nombre de séjours"
LABEL_TX_SEJ = "Taux de recours standardisé /100 000 hab."
LABEL_ANNEE = "Année"
LABEL_FINESS = "finess"
LABEL_FINESS_GEO = "num_finess_et"
LABEL_NOM_ES = "Etablissement"
LABEL_REGION = "Région"
LABEL_DEPARTEMENT = "Département"

LABEL_TAUX_SEJOURS = "Taux de recours standardisé / 100 000 habitants"
LABEL_TAUX_SEJOURS_COURS = "Taux de recours standardisé <br> / 100 000 hab."

PLOTLY_SHOW_CONFIG = {
    "toImageButtonOptions": {
        "format": "png",  # one of png, svg, jpeg, webp
        "filename": "custom_image",
        "height": 600,
        "width": 900,
        "scale": 2,  # Multiply title/legend/axis/canvas sizes by this factor
    }
}
