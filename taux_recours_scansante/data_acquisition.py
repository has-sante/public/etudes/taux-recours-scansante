from dataclasses import dataclass
from os import path
import os
from typing import Dict, List

import numpy as np
import pandas as pd
import requests
import logging

logger = logging.getLogger()

from taux_recours_scansante.constants import (
    DIR2DATA,
    DIR2OPEN_CCAM,
    LABEL_ANNEE,
    LABEL_CODE_ACTE,
    LABEL_GROUPE_ACTE,
    LABEL_NB_SEJ,
    LABEL_TX_SEJ,
)

niveaux_geo = ["dep", "reg"]
versions_liste = [
    "v2019",
    "v2020",
    "v2021",
    "v2022",
]  # v2022 pour la période 2018-2022, v2021 pour période 2017-2021, v2020 pour période 2016-2020, v2019 pour période 2015-2019
# Note:
# - v2021 n'est utilisé que pour l'année 2017, les années ultérieures sont récupérer dans l'année 2018 (cf. function `format_table`).
# - v2019 n'est utilisé que pour l'année 2015, le reste des années est récupéré par v2020  (cf. function `format_table`).


@dataclass
class ConfigurationEtude:
    nom_etude: str
    actes_dict: Dict[str, List[str]]
    secure_data: bool = False


def get_data_from_scan_sante(configuration_etude: ConfigurationEtude) -> None:
    # Création du dossier d'étude si inexistant
    dir2etude = DIR2DATA / configuration_etude.nom_etude
    (dir2etude).mkdir(exist_ok=True, parents=True)

    # Téléchargement des données depuis scan santé
    for acte, geo, version in iter_acte_geo_version(
        actes_dict=configuration_etude.actes_dict
    ):
        download_html(
            acte,
            geo,
            version,
            nom_etude=configuration_etude.nom_etude,
            secure_data=configuration_etude.secure_data,
        )

    # Lecture des tables, compte des lignes et suppression des fichiers en erreur
    for acte, geo, version in iter_acte_geo_version(
        actes_dict=configuration_etude.actes_dict
    ):
        file_name = get_filepath(
            acte=acte,
            geo=geo,
            version=version,
            nom_etude=configuration_etude.nom_etude,
            secure_data=configuration_etude.secure_data,
        )
        print(file_name)
        with open(file_name) as f:
            try:
                l = pd.read_html(f, decimal=",", thousands=" ")
                df = l[2]
                print(len(df), "lignes dans la table")
            except:
                print("erreur")
                os.remove(file_name)
    # formattage
    df_list = []
    for acte, geo, version in iter_acte_geo_version(
        actes_dict=configuration_etude.actes_dict
    ):
        df_list.append(
            format_table(
                acte,
                geo,
                version,
                nom_etude=configuration_etude.nom_etude,
                actes_dict=configuration_etude.actes_dict,
                secure_data=configuration_etude.secure_data,
            )
        )
    df = pd.concat(df_list)

    np.testing.assert_array_equal(
        df.index.duplicated(),  # Doublons dans l'index
        np.array(
            df.reset_index().duplicated()
        ),  # Doublons sur toutes les colonnes (dont index)
    )  # Vérifier que les lignes en doublon dans l'index ont des valeurs identiques sur les lignes

    df = df[
        ~df.index.duplicated()
    ]  # Suppression des lignes France, en doublon depuis les tables departement et région

    file_name = "taux-recours"
    if configuration_etude.secure_data:
        file_name += "-secure"
    print(file_name)
    df.to_csv(dir2etude / (file_name + ".csv"))
    df.to_excel(dir2etude / (file_name + ".xlsx"))
    df.to_parquet(dir2etude / (file_name + ".parquet"))


def get_acte_groupe(acte: str, actes_dict: Dict[str, List[str]]):
    for groupe_acte, liste_acte in actes_dict.items():
        if acte in liste_acte:
            return groupe_acte
    return None


def iter_acte_geo_version(actes_dict: Dict[str, List[str]]):
    for groupe_acte, liste_actes in actes_dict.items():
        for acte in liste_actes:
            for geo in niveaux_geo:
                for version in versions_liste:
                    yield acte, geo, version


def get_filepath(
    acte: str,
    geo: str,
    version: str,
    nom_etude: str,
    secure_data: bool = False,
):
    if secure_data:
        return DIR2DATA / nom_etude / f"taux-recours-secure-{acte}-{geo}-{version}.html"
    else:
        return DIR2DATA / nom_etude / f"taux-recours-{acte}-{geo}-{version}.html"


def get_url(acte, geo, version, secure_data: bool = False):
    base_url_part = "www"
    if secure_data:
        base_url_part = "secure"
    return (
        f"https://{base_url_part}.scansante.fr/applications/taux-de-recours-mco/submit?snatnav=&mbout=part1&"
        + f"champ=MCO&unite=s%25E9jour&version={version}&taux=stand&tgeo={geo}&stat1=&IPA=&ASO=&CAS=&DA=&GP=&GA=&RAC=&"
        + f"type_rgp1=ACTE&S_F=&M_F=&Act={acte}&version2={version}&tgeo2=fe&codegeo=fe&stat2=&type_rgp2=tous"
    )


def download_html(acte, geo, version, nom_etude: str, secure_data: bool = False):
    filepath = get_filepath(
        acte, geo, version, nom_etude=nom_etude, secure_data=secure_data
    )
    url = get_url(acte, geo, version, secure_data=secure_data)

    if path.exists(filepath):
        print(f"Le fichier '{filepath}' existe déjà")
    else:
        print(f"Téléchargement de la page {url}")
        r = requests.get(url, headers={"User-Agent": "Mozilla/5.0 Firefox/100.0"})
        if r.status_code != 200:
            raise Exception(f"Erreur pour télécharger la page : {r.status_code}")
        print(f"Ecriture sur le fichier '{filepath}'")
        with open(filepath, "w") as f:
            f.write(r.text)


def format_table(
    acte,
    geo,
    version,
    nom_etude: str,
    actes_dict: Dict[str, List[str]],
    secure_data: str = False,
):
    filepath = get_filepath(
        acte, geo, version, nom_etude=nom_etude, secure_data=secure_data
    )
    with open(filepath) as f:
        l = pd.read_html(f, decimal=",", thousands=" ")

    df = l[2]

    df[LABEL_GROUPE_ACTE] = get_acte_groupe(acte, actes_dict=actes_dict)
    df[LABEL_CODE_ACTE] = acte

    df["code_geo"] = df.iloc[:, 0].str.split(" - ").str[0]
    df.iloc[-1, -1] = "FR"

    df["libelle_geo"] = df.iloc[:, 0].str.split(" - ").str[1]
    df.iloc[-1, -1] = "France"

    df["niveau_geo"] = "Region" if geo == "reg" else "Departement"
    df.iloc[-1, -1] = "France"

    df = df.iloc[:, 1:].set_index(
        [
            LABEL_GROUPE_ACTE,
            LABEL_CODE_ACTE,
            "niveau_geo",
            "code_geo",
            "libelle_geo",
        ]
    )
    df.columns.names = ["variable", LABEL_ANNEE]
    df = df.stack()

    # Tranformer 'annee' en entier
    df = df.reset_index(LABEL_ANNEE)
    df[LABEL_ANNEE] = df[LABEL_ANNEE].astype(int)
    df = df.set_index([df.index, df[LABEL_ANNEE]])

    map_columns = {
        "Nombre de séjour": LABEL_NB_SEJ,
        "Taux de séjour Standardisé /1000 hab.": LABEL_TX_SEJ,
        "Indice national": "indice_national",
        "Indice régional": "indice_regional",
    }

    df = df.rename(columns=map_columns)
    if geo == "reg":
        df["indice_regional"] = np.nan
    if geo == "dep":
        df.loc[
            df.index.get_level_values("niveau_geo") == "France",
            "indice_regional",
        ] = np.nan  # Correction erreur '0'

    df[LABEL_TX_SEJ] = (
        df[LABEL_TX_SEJ].replace("N.S.", np.nan).astype(float) * 100
    )  # taux standardisé /100000 hab.
    df[LABEL_NB_SEJ] = (
        df[LABEL_NB_SEJ].replace("1 à 10", np.nan).astype(float).astype("Int64")
    )
    df = df[map_columns.values()]

    # L'ATIH a les données en doublon pour les années 2018 à 2021 dans les deux versions de l'API (v2021 et 2022).
    # Celles-ci semblent identiques (testé sur deux exemples France entière et AIN).
    # Je prends les données 2017 dans l'API v2021 et les autres années dans la v2022.
    if version == "v2021":
        df = df.loc[df.index.get_level_values(LABEL_ANNEE) == 2017]
    if version == "v2019":
        df = df.loc[df.index.get_level_values(LABEL_ANNEE) == 2015]
    return df


def get_open_ccam(annee: int, niveau_geo: str = "Region"):
    """
    Télécharge la donnée d'open CCAM pour l'année d'intérêt au niveau géographique ciblé.

    Url type : https://www.scansante.fr/sites/www.scansante.fr/files/content/542/open_ccam_22_reg.csv

    Args:
        annee (int): _description_
    """
    if niveau_geo == "Region":
        annee_2_digits = int(np.floor(annee / 100))

        open_ccam_url = f"https://www.scansante.fr/sites/www.scansante.fr/files/content/542/open_ccam_{annee_2_digits}_reg.csv"
        path2open_ccam = DIR2OPEN_CCAM / f"open_ccam_{niveau_geo}_{annee}.csv"
        DIR2OPEN_CCAM.mkdir(exist_ok=True, parents=True)
        open_ccam_df = pd.read_csv(open_ccam_url)
        open_ccam_df.to_parquet(path2open_ccam)

    else:
        raise NotImplementedError(
            "Seulement niveau_geo==Region est implémenté actuellement."
        )
    return open_ccam_df
