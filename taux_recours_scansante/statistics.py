import numpy as np
import pandas as pd

from taux_recours_scansante.constants import (
    LABEL_ANNEE,
    LABEL_GROUPE_ACTE,
    LABEL_NB_SEJ,
    LABEL_TX_SEJ,
)


def coefficient_variation(x):
    return np.std(x, ddof=0) / np.mean(x) * 100


def calcul_statistiques(df: pd.DataFrame, annee, metropole=False):
    DROM = [
        "LA RÉUNION",
        "MARTINIQUE",
        "GUYANE",
        "MAYOTTE",
        "GUADELOUPE",
        "SAINT-BARTHÉLEMY",
        "SAINT-MARTIN",
    ]
    if metropole:
        print(f"Met de côté les DROM: {DROM}")
        base_df = df.drop(DROM, level="libelle_geo", errors="ignore")
    else:
        base_df = df

    df_groupe_acte = (
        base_df.xs(annee, level=LABEL_ANNEE)
        .groupby(level=[LABEL_GROUPE_ACTE, "niveau_geo", "code_geo"])
        .sum()
    )

    result_list = []

    niveau_geo = "France"
    result = df_groupe_acte.xs(niveau_geo, level="niveau_geo").droplevel(["code_geo"])
    result[LABEL_NB_SEJ] = result[LABEL_NB_SEJ].astype(int)
    result.columns = pd.MultiIndex.from_arrays(
        [
            [niveau_geo] * 2,
            [LABEL_NB_SEJ, LABEL_TX_SEJ],
        ]
    )
    result_list.append(result)

    niveau_geo = "Departement"
    result = (
        df_groupe_acte.xs(niveau_geo, level="niveau_geo")
        .groupby(LABEL_GROUPE_ACTE)
        .agg(
            {
                LABEL_NB_SEJ: np.sum,
                LABEL_TX_SEJ: [np.mean, np.std, coefficient_variation],
            }
        )
        .round(1)
    )
    result.columns = pd.MultiIndex.from_arrays(
        [
            ["France"] + [niveau_geo] * 3,
            [
                "nb_sejour_localises",
                "moyenne",
                "ecart_type",
                "coefficient_variation",
            ],
        ]
    )
    result_list.append(result)

    niveau_geo = "Region"
    result = (
        df_groupe_acte.xs(niveau_geo, level="niveau_geo")
        .groupby(LABEL_GROUPE_ACTE)[LABEL_TX_SEJ]
        .agg([np.mean, np.std, coefficient_variation])
        .round(1)
    )
    result.columns = pd.MultiIndex.from_arrays(
        [[niveau_geo] * 3, ["moyenne", "ecart_type", "coefficient_variation"]]
    )
    result_list.append(result)
    return pd.concat(result_list, axis=1)
