import json
from taux_recours_scansante.constants import (
    DIR2DATA,
    LABEL_ANNEE,
    LABEL_FINESS,
    LABEL_FINESS_GEO,
    LABEL_GROUPE_ACTE,
    LABEL_NOM_ES,
    LABEL_TX_SEJ,
    LABEL_TAUX_SEJOURS,
    LABEL_TAUX_SEJOURS_COURS,
)
from taux_recours_scansante.statistics import calcul_statistiques
import pandas as pd

import plotly.express as px
from plotly.graph_objs._figure import Figure


def dessine_carte_recours(
    df: pd.DataFrame,
    annee: int,
    groupe_acte: str,
    niveau_geo: str,
    metropole=True,
) -> Figure:
    """
    Crée une carte des taux de recours à partir des données de scan-santé.

    Args:
        df (pd.DataFrame): _description_
        annee (int): Année étudiée
        groupe_acte (str): groupe d'actes
        niveau_geo (str): Region ou Departement
        metropole (bool, optional): Si True, exclue les DROM de la carte. Defaults to True.
    """

    # https://france-geojson.gregoiredavid.fr/
    geojson_dict = dict()
    if niveau_geo == "Region":
        with open(DIR2DATA / "regions.geojson") as f:
            geojson_dict["Region"] = json.load(f)
    else:
        with open(DIR2DATA / "departements.geojson") as f:
            geojson_dict["Departement"] = json.load(f)

    if groupe_acte is None:
        df_geo = (
            df.droplevel(LABEL_GROUPE_ACTE)
            .xs((niveau_geo, annee), level=["niveau_geo", LABEL_ANNEE])[LABEL_TX_SEJ]
            .groupby(["code_geo", "libelle_geo"])
            .sum()
            .reset_index()
        )

        moyenne = df_geo[LABEL_TX_SEJ].mean()
        ecart_type = df_geo[LABEL_TX_SEJ].std()
    else:
        # TODO: Simplify by directly computing those with df_geo ?
        stats = calcul_statistiques(df=df, annee=annee, metropole=metropole)
        moyenne = stats["France"][LABEL_TX_SEJ][groupe_acte]
        ecart_type = stats[niveau_geo]["ecart_type"][groupe_acte]

        df_geo_filtered = df.xs((niveau_geo), level="niveau_geo")
        all_code_libelle_geo = df_geo_filtered.reset_index()[
            ["libelle_geo", "code_geo"]
        ].drop_duplicates()
        #
        df_geo = (
            df_geo_filtered.xs(
                (annee, groupe_acte), level=[LABEL_ANNEE, LABEL_GROUPE_ACTE]
            )
            .reset_index()
            .groupby(["code_geo", "libelle_geo"])[LABEL_TX_SEJ]
            .sum()
            .reset_index()
            .fillna(0)
        )
        df_geo = df_geo.merge(
            all_code_libelle_geo, on=["libelle_geo", "code_geo"], how="right"
        ).fillna(0)

    if niveau_geo == "Region":
        df_geo = (
            df_geo.set_index("code_geo")
            .drop(["01", "02", "03", "04", "06"], errors="ignore")
            .reset_index()
        )
    df_geo["libelle_geo"] = df_geo["code_geo"] + " - " + df_geo["libelle_geo"]

    fig = px.choropleth(
        df_geo,
        geojson=geojson_dict[niveau_geo],
        color=LABEL_TX_SEJ,
        locations="code_geo",
        hover_name="libelle_geo",
        featureidkey="properties.code",
        projection="mercator",
        color_continuous_scale="Viridis_r",
        range_color=(moyenne - ecart_type, moyenne + ecart_type),
        #    legend
    )
    fig.update_geos(fitbounds="locations", visible=False)
    fig.update_layout(
        coloraxis_colorbar=dict(
            title=LABEL_TAUX_SEJOURS_COURS,
        ),
        margin={"r": 0, "t": 0, "l": 0, "b": 0},
    )
    return fig


def get_coordonees_geo(
    finess_df: pd.DataFrame,
    activite_centres: pd.DataFrame,
    drop_drom: bool = True,
):
    """Récupère les coordonnées latitude/longitude pour les établissements en se
    basant sur finess et une correspondance manuelle pour les établissements juridiques.

    Args:
        finess_df (pd.DataFrame): _description_
        activite_centres (pd.DataFrame):_description_
        drop_drom (bool, optional): _description_. Defaults to
        True.

    Returns:
        _type_: _description_
    """
    min_thres = 5
    activite_centres_sup_thres = activite_centres.loc[
        activite_centres["Total général"] >= min_thres
    ]
    activite_centres_sup_thres[LABEL_FINESS] = activite_centres_sup_thres[
        "Étiquettes de lignes"
    ].map(lambda x: x.split("-")[0].strip())
    activite_centres_sup_thres[LABEL_NOM_ES] = activite_centres_sup_thres[
        "Étiquettes de lignes"
    ].map(lambda x: x.split("-")[1])
    # Fichier de correspondance (manuelle) avec un finess établissement pour les
    # CHUS et les établissement pourlesquels le finess donné dans le fichier
    # d'activité est un finess juridique (sans correspodance géographique dans finess)
    map_finess_ej2finess_geo = pd.read_csv(
        DIR2DATA / "mapping_finess_ej2geo.csv",
        dtype={"id_fi_ej": str, "id_fi": str},
    )
    activite_centres_finess_geo = activite_centres_sup_thres.merge(
        map_finess_ej2finess_geo,
        left_on=LABEL_FINESS,
        right_on="id_fi_ej",
        how="left",
    )
    activite_centres_finess_geo[LABEL_FINESS_GEO] = activite_centres_finess_geo["id_fi"]
    mask_non_chu = activite_centres_finess_geo[LABEL_FINESS_GEO].isna()
    activite_centres_finess_geo.loc[
        mask_non_chu, LABEL_FINESS_GEO
    ] = activite_centres_finess_geo.loc[mask_non_chu, LABEL_FINESS]

    activite_centres_tavi_geoloc = activite_centres_finess_geo.merge(
        finess_df[
            [
                "num_finess_ej",
                "num_finess_et",
                "raison_sociale_et",
                "raison_sociale_longue_et",
                "latitude",
                "longitude",
            ]
        ],
        on="num_finess_et",
        how="left",
    ).rename(columns={"Total général": "nb_tavi_total"})
    if drop_drom:
        activite_centres_tavi_geoloc = activite_centres_tavi_geoloc[
            ~activite_centres_tavi_geoloc["num_finess_ej"].str.startswith("97")
        ]
    return activite_centres_tavi_geoloc
