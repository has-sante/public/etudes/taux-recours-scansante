

```{eval-rst}
.. toctree::
   :maxdepth: 1
   
   readme
   Données <data>
   Usage <usage>
   Rapports <reports/index>
   License <license>
```

```{eval-rst}
.. role:: bash(code)
   :language: bash
   :class: highlight
```
