# Documentation des données

[Documentation des données sources par l'ATIH](_static/notice_taux_recours_mco_octobre19.pdf).

## Préparation

L'acquisition des données est réalisée
- en téléchargeant les pages web (fichiers html) de www.scansante.fr, pour
   - chaque code acte
   - les versions `2016` (période 2012-2016), `2021` (période 2017) et `2022` (période 2018-2022)
   - les niveaux géographiques `département` et `région` (nouvelles régions)
- en extrayant les tables depuis les pages web
- en mettant en forme et combinant les tables dans une table unique

> 📝 **Note**
> Une option `secure` permet de télécharger puis de travailler sur les données de `secure.scansante.fr`.
> Cette version de Scan Santé n'applique pas le secret statistique, et fournit les valeurs exactes entre 1 et 10.
> Ce site n'est accessible que sur la plateforme des données hospitalières, ou sur le réseau interne de l'ATIH.

Les nettoyages de données réalisés sont
- remplacement des valeurs "N.S." (non significatif) et "1 à 10" par valeur manquante (NA)
- le taux est multiplié par 100, pour correspondre à un taux pour 100k habitants
- les lignes "France" récupérées en doublons pour le même acte, depuis la page département et la page région, sont supprimées.

## Schéma

| Axe ou variable | Colonne| Libellé | Type |
|-|-|-|-|
| Axe | groupe_acte  | Regroupement d'actes pour l'analyse | string |
| Axe | code_acte | Code de l'acte | string |
| Axe | niveau_geo  | Niveau géographique : France, Region ou Departement | string |
| Axe | code_geo | Code géographique officiels région et département. FR pour la France. | string |
| Axe | libelle_geo  | Libellé géographique | string |
| Axe | annee | Année| integer |
| Variable | nombre_sejour | Nombre de séjour | integer |
| Variable | taux_sejour  | Taux de séjour standardisé pour 100 000 habitants | number |
| Variable | indice_national | Rapport entre le taux de sejour standardisé du niveau géographique et de la France (non défini au niveau France) | number |
| Variable | indice_regional | Rapport entre le taux de sejour standardisé du niveau géographique et de la région (non défini au niveau France et Région) | number |
