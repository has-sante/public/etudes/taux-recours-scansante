# Rapports

```{eval-rst}
.. toctree::
   :maxdepth: 1
   
   Rapport d'analyse TAVI, année 2022 <2023-01-09-statistiques-tavi>
   Rapport d'analyse chirurgie tendinopathie de l'épaule, années 2015-2022 <2023-11-28-statistiques-chirurgie-tendinopathie>
```

## Contexte du rapport TAVI, année 2022 

[Rapport d'analyse Tavi](https://has-sante.pages.has-sante.fr/public/etudes/taux-recours-scansante/reports/2023-01-09-statistiques-tavi.html)

### Description

Cette analyse a été mobilisée pour le rapport [Critères d’éligibilité des centres implantant des TAVIs (2023)](https://www.has-sante.fr/upload/docs/application/pdf/2023-12/rapport_tavi_version_definitive_2023-12-18_11-48-12_768.pdf). 

#### Contexte

Depuis 2009, l’activité d’implantation des bioprothèses valvulaires aortiques implantées par voie trans-cathéter (TAVI) est strictement encadrée :

- d’abord, pour une période de 2 ans avec la publication d’une liste nominative
de 33 centres et l’obligation de recueillir les données cliniques de tous les
patients implantés en France en situation réelle d’utilisation (arrêté du 29
décembre 2009). Cela a mené à la création du registre France 2 ;

- ensuite, par la publication d’un cahier des charges décrivant les critères de
compétences et de moyens auquel devaient répondre les centres implanteurs
(arrêté du 3 juillet 2012). Cet arrêté a été prorogé à plusieurs reprises et a
évolué au cours du temps, à la marge, à la suite de propositions figurant dans
les différentes évaluations produites par la HAS1. La dernière prorogation
date de 2020 avec la publication de l’arrêté du 16 décembre avec une date de fin
de validité des critères fixée au 31 décembre 2023. L’encadrement est délégué
aux Agences Régionales de Santé (ARS) qui ont pour mission de contrôler le
respect des critères. En parallèle, l’instruction DGOS du 7 mars 2013 précise
les modalités de recueil de données cliniques complémentaires obligatoires par
l’intermédiaire des Observatoires du Médicament, des Dispositifs médicaux et le
l’Innovation Thérapeutique (OMEDIT). La collection de ces variables est réalisée
par le biais du registre France-TAVI, registre réalisé sous l’égide de la
Société Française de Cardiologie, et du rapport annuel des OMEDIT.
    
Le principal critère d’encadrement existant est la nécessité de pouvoir disposer
sur site des départements de cardiologie interventionnelle et de chirurgie
cardiaque, en cas de conversion chirurgicale des patients en urgence.
Aujourd’hui, tous les centres en France réalisant de la chirurgie cardiaque sont
ouverts à l’activité de pose des TAVIs. Cela correspond à une cinquantaine
d’établissements de santé.

À ce jour, au regard de la démocratisation de cette procédure, de l’extension de
ses indications et de l’amélioration des résultats cliniques, il existe une
forte attente de la part de la communauté cardiologique pour voir ouvrir
l’activité de pose de TAVIs à de nouveaux centres ne disposant pas sur site de
département de chirurgie cardiaque.

### Périmètre

- Période : depuis 2018 (avant l’extension d’indications) jusqu’à 2022
- Niveau géographique : départemental et régional
- Actes TAVI : 2 actes
    - DBLF001 : Pose d’une bioprothèse de la valve aortique, par voie artérielle transcutanée (acte très utilisé, plus de 15 000 par an)
    - DBLA004 : Pose d’une bioprothèse de la valve aortique, par abord de l’apex du cœur par thoracotomie sans CEC (acte très peu utilisé environ 300 en 2018 et moins de 200 actes depuis 2020)

- Chirurgie cardiaque : 14 actes

    - DBKA006 :	Remplacement de la valve aortique par prothèse mécanique ou bioprothèse avec armature, par thoracotomie avec CEC
    - DBKA003 :	Remplacement de la valve aortique par bioprothèse sans armature, par thoracotomie avec CEC
    - DBKA001 :	Remplacement de la valve aortique par homogreffe, par thoracotomie avec CEC
    - DBKA011 :	Remplacement de la valve aortique par prothèse en position non anatomique, par thoracotomie avec CEC
    - DBKA009 :	Remplacement de la valve aortique et de la valve atrioventriculaire gauche par prothèse mécanique ou par bioprothèse avec armature, par thoracotomie avec CEC
    - DBMA009 :	Reconstruction de l'anneau aortique avec remplacement de la valve par prothèse mécanique ou bioprothèse avec armature, par thoracotomie avec CEC
    - DBMA010 :	Reconstruction de l'anneau aortique avec remplacement de la valve par homogreffe, par thoracotomie avec CEC
    - DBMA006 :	Reconstruction de l'anneau aortique avec remplacement de la valve par bioprothèse sans armature, par thoracotomie avec CEC
    - DBMA001 :	Reconstruction de la voie aortique par élargissement antérodroit de l'anneau avec remplacement de la valve, par thoracotomie avec CEC
    - DBMA015 :	Reconstruction de la voie aortique par élargissement antérogauche de l'anneau et ouverture de l'infundibulum pulmonaire, avec remplacement de la valve, par thoracotomie avec CEC
    - DGKA011 :	Remplacement de l'aorte thoracique ascendante avec remplacement de la valve aortique, sans réimplantation des artères coronaires, par thoracotomie avec CEC
    - DGKA015 :	Remplacement de l'aorte thoracique ascendante avec remplacement de la valve aortique, avec réimplantation des artères coronaires, par thoracotomie avec CEC
    - DGKA018 :	Remplacement de l'aorte thoracique ascendante et de l'aorte horizontale avec remplacement de la valve aortique, sans réimplantation des artères coronaires, par thoracotomie avec CEC
    - DGKA014 :	Remplacement de l'aorte thoracique ascendante et de l'aorte horizontale avec remplacement de la valve aortique, avec réimplantation des artères coronaires, par thoracotomie avec CEC

- Mesure : taux de recours standardisés (âge et sexe)
- Source des données : l'application [Taux de recours MCO](https://www.scansante.fr/applications/taux-de-recours-mco/) du site ScanSanté, développé par l'Atih, qui restitue des taux de recours standardisés à partir des données du PMSI

### Analyse

Différentes restitutions sont disponibles concernant le volume d'activité et les taux de recours standardisés des groupes d'actes `TAVI` et `chirurgie cardiaque` dans [le rapport interactif d'analyse](https://has-sante.pages.has-sante.fr/public/etudes/taux-recours-scansante/reports/index.html):

- La moyenne française des taux de recours standardisés et du nombre de séjours en 2022 par
  groupe d'actes, ainsi que la dispersion par région et département.
- La variabilité dans le temps de l'activité en nombre de séjours par acte CCAM.
- La variabilité géographique du taux de recours standardisé pour l'année 2022, par département et par région pour la chirurgie cardiaque et le TAVI.
- La variabilité temporelle des taux de recours standardisés par groupe d'acte: en distinguant par région et par département.
- Les cartes de taux de recours standardisés pour les actes de TAVI, avec représentation des centres d'activité TAVI (carte libre de droit).

## Contexte du rapport chirurgie tendinopathie de l'épaule, années 2015-2022

[Rapport d'analyse chirurgie pour la tendinopathie de l'épaule](https://has-sante.pages.has-sante.fr/public/etudes/taux-recours-scansante/reports/2023-11-28-statistiques-chirurgie-tendinopathie.html)


Cette analyse a été présentée aux experts du groupe de travail HAS sur la chirurgie de l'épaule en cas de tendinopathie en décembre 2023 à la suite de l'étude sur la [Description des parcours de soins préopératoires des adultes de 40 ans et plus opérés d’une acromioplastie isolée en 2022 à partir des données du SNDS](https://www.has-sante.fr/upload/docs/application/pdf/2023-09/etat_des_pratiques.pdf). L'objectif était de mieux comprendre les liens entre offre et demande pour ces actes de chirurgies. 

Malheureusement, l'information sur l'offre de soin --la part de chirurgiens orthopédiques faisant des opérations de l’épaule-- n'est pas disponible simplement. L'analyse n'a donc pas été utilisée par les experts.