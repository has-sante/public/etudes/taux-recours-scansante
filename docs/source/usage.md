# Utilisation

## Installation des dépendances du projet

A ne faire qu'une seule fois:

1. 🚧 Dans l'onglet Launcher, cliquez sur "Terminal
2. 🚧 Entrez les commandes suivantes (une à une)

```shell
cd taux-recours-scansante
make provision-environment
poetry run python -m ipykernel install --user --name scansante
```

## Acquisition des données

L'acquisition des données se fait en lançant le script depuis la cli en ligne de commande en précisant le nom de l'étude. Actuellement seul les études suivantes sont disponibles: "tavi", "tendinopathie-epaule", "chirurgie-tendinopathie". Par exemple pour lancer le téléchargement des données sur le tavi, il faut lancer la commande: 

`poetry run cli download-data -e tavi`

Cette commande télécharge les données sur le site de l'ATIH et sauvegarde des fichiers parquet/csv et excel `taux-recours` dans le dossier `data/tavi/`.

## Analyse statistique

Une analyse standardisée des taux de recours est proposées dans un notebook d'anlyseprenant en e projet s'exécute en faisant tourner le notebook d'analyse statistiques. Deux solutions pour visualiser l'analyse statistiques: 

- Ouvrir le notebook d'étude (ex. '2023-01-09-statistiques-tavi.ipynb') dans son éditeur de notebook favori (vscode ou jupyter).
- Les notebooks peuvent être intégrés à la documentation en lançant la commande make suivante:  `make notebooks-docs`. Pour l'instant, seul celui sur le TAVI et celui sur la chirugie en cas de tendinopathie de l'épaule ont été intégrés. Ensuite, construire la documentation permet de visualiser le notebook : `make docs-html`.

> 📝**Note**
> Un notebook d'acquisition de données est également disponible pour détailler chaque étape de l'acquisition.


## Ajout d'une nouvelle étude

Pour créer une nouvelle étude, il suffit d'ajouter un dictionnaire de configuration contenant les groupes d'actes à étudier dans le fichier [constants.py](https://gitlab.has-sante.fr/has-sante/public/etudes/taux-recours-scansante/-/tree/main/taux_recours_scansante/constants.py). Se baser sur les dictionnaires existants pour la syntaxe.


## Utiliser le projet sur le datalab de la HAS

Cette partie est décrite dans le [wiki interne de la mission data](https://has-sante.pages.has-sante.fr/wiki/Tutoriels/DataLab/).