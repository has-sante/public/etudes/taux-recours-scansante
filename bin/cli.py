#! /usr/bin/env python
import click
from dotenv import load_dotenv

from taux_recours_scansante.constants import ETUDES_DISPONIBLES

from taux_recours_scansante.data_acquisition import (
    ConfigurationEtude,
    get_data_from_scan_sante,
)

# see `.env` for requisite environment variables
load_dotenv()


@click.group()
@click.version_option(package_name="taux_recours_scansante")
def cli():
    pass


@cli.command()
@click.option(
    "-e",
    "--nom_etude",
    type=click.Choice(list(ETUDES_DISPONIBLES.keys()), case_sensitive=False),
    help="""Télécharge, formatte et sauvegarde les données pour une étude depuis scan-sante.
Les données sont aggrégées au [format présentée sur la documentation du projet](https://has-sante.pages.has-sante.fr/public/etudes/taux-recours-scansante/data.html#schema).
    """,
)
def download_data(nom_etude: str) -> None:
    configuration_etude = ConfigurationEtude(
        nom_etude=nom_etude,
        actes_dict=ETUDES_DISPONIBLES[nom_etude],
        secure_data=False,
    )
    get_data_from_scan_sante(configuration_etude=configuration_etude)


if __name__ == "__main__":
    cli()
